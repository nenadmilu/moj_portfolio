document.querySelector(".proveri").addEventListener("click", function () {
    let ocenaSrpskiJezik = Number(document.querySelector(".srpskiJezik").value);
    let ocenaMatematika = Number(document.querySelector(".matematika").value);
    let ocenaFizika = Number(document.querySelector(".fizika").value);
    let ocenaHemija = Number(document.querySelector(".hemija").value);
    let ocenaIstorija = Number(document.querySelector(".istorija").value);
    let srednjaOcena = ((ocenaSrpskiJezik + ocenaMatematika + ocenaFizika + ocenaHemija + ocenaIstorija) / 5).toFixed(2);
    if (ocenaSrpskiJezik < 1 || ocenaSrpskiJezik > 5 ||
        ocenaMatematika < 1 || ocenaMatematika > 5 ||
        ocenaFizika < 1 || ocenaFizika > 5 ||
        ocenaHemija < 1 || ocenaHemija > 5 ||
        ocenaIstorija < 1 || ocenaIstorija > 5) {
            document.querySelector(".upozorenjeUspeh").innerHTML = "Uneli ste jednu ili više nepostojećih ocena ili niste upisali sve ocene";
        }
        else if (ocenaSrpskiJezik == 1 || ocenaMatematika == 1 || ocenaFizika == 1 || ocenaHemija == 1 || ocenaIstorija ==1) {
            document.querySelector(".upozorenjeUspeh").innerHTML = "Nedovoljan (jedna ili više jedinica)";
        }
        else if (srednjaOcena >= 4.50) {
            document.querySelector(".upozorenjeUspeh").innerHTML = "Odličan"
            document.querySelector(".prosek").innerHTML = srednjaOcena
        }
        else if (srednjaOcena >= 3.50 && srednjaOcena < 4.50) {
            document.querySelector(".upozorenjeUspeh").innerHTML = "Vrlo dobar"
            document.querySelector(".prosek").innerHTML = srednjaOcena
        }
        else if (srednjaOcena >= 2.50 && srednjaOcena < 3.50) {
            document.querySelector(".upozorenjeUspeh").innerHTML = "Dobar"
            document.querySelector(".prosek").innerHTML = srednjaOcena
        }
        else if (srednjaOcena >= 1.50 && srednjaOcena < 2.50) {
            document.querySelector(".upozorenjeUspeh").innerHTML = "Dovoljan"
            document.querySelector(".prosek").innerHTML = srednjaOcena
        }
})

document.querySelector(".restart").addEventListener("click", function () {
    document.location.reload(true);
})