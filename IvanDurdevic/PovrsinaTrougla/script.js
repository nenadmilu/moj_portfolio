document.querySelector(".proveri").addEventListener("click", function () {
    let a = Number(document.querySelector(".aStranica").value);
    let b = Number(document.querySelector(".bStranica").value);
    let c = Number(document.querySelector(".cStranica").value);
    let o = (a + b + c) / 2
    let površina = (o * (o - a) * (o - b) * (o - c)) ** 0.5
    // Može i let površina = ((a+b-c)*(a-b+c)*(-a+b+c)*(a+b+c))**0.5/4
    if (a <= 0 || b <= 0 || c <= 0 || a >= b + c || b >= a + c || c >= a + b) {
        document.querySelector(".rešenje").innerHTML = "Nemoguće dimenzzije stranica za trougao ili nisu unete sve stranice.";
    }
    else document.querySelector(".rešenje").innerHTML = `Površina je ${površina}.`
})

document.querySelector(".restart").addEventListener("click", function () {
    document.location.reload(true);
})